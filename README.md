# ShaderDemo合集

#### 介绍
shader练习项目展示

unity版本：2020.3.25f1c1

#### 一、崩坏3角色渲染还原(粗糙版)

主要shader简析 [点击查看wiki](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%8D%A1%E9%80%9A%E6%B8%B2%E6%9F%93/%E5%B4%A9%E5%9D%8F3/%E5%B4%A9%E5%9D%8F3%E8%BF%98%E5%8E%9F%E4%B8%BB%E8%A6%81shader%E8%A7%A3%E6%9E%90)


#### 二、环境渲染
深度贴图(脚印): 已完成，待优化调整 [点击查看wiki](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%8D%A1%E9%80%9A%E6%B8%B2%E6%9F%93/%E7%8E%AF%E5%A2%83%E6%B8%B2%E6%9F%93/%E8%84%9A%E5%8D%B0(%E6%B7%B1%E5%BA%A6%E8%B4%B4%E8%8A%B1)?sort_id=5229832)

地面裂缝(基于深度和渲染队列)：[点击查看wiki](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E7%8E%AF%E5%A2%83%E6%B8%B2%E6%9F%93/%E5%9C%B0%E9%9D%A2%E8%A3%82%E7%BC%9D(%E5%9F%BA%E4%BA%8E%E6%B7%B1%E5%BA%A6%E5%A4%84%E7%90%86))

体积云 [点击查看wiki](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E7%8E%AF%E5%A2%83%E6%B8%B2%E6%9F%93/%E4%BD%93%E7%A7%AF%E4%BA%91)


地面塌陷(视差映射)
水体、岩浆、草地、 准备做的内容(类荒野之息/原神 的还原)

 **_效果待做_** 



角色与环境交互(草地、布料) [wiki补充示例](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%BE%85%E5%81%9A%E6%95%88%E6%9E%9C/%E8%A7%92%E8%89%B2%E4%B8%8E%E7%8E%AF%E5%A2%83%E4%BA%A4%E4%BA%92?sort_id=5287106)

3d 透视小地图 [wiki补充示例](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%BE%85%E5%81%9A%E6%95%88%E6%9E%9C/3d%20%E9%80%8F%E8%A7%86%E5%B0%8F%E5%9C%B0%E5%9B%BE?sort_id=5287108)

类塞尔达使用磁铁环境修改效果 [wiki补充示例](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%BE%85%E5%81%9A%E6%95%88%E6%9E%9C/%E7%B1%BB%E5%A1%9E%E5%B0%94%E8%BE%BE%E4%BD%BF%E7%94%A8%E7%A3%81%E9%93%81%E7%8E%AF%E5%A2%83%E4%BF%AE%E6%94%B9%E6%95%88%E6%9E%9C%20?sort_id=5287110)

LOL 小龙击杀地形变化效果 [wiki补充示例](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%BE%85%E5%81%9A%E6%95%88%E6%9E%9C/LOL%20%E5%B0%8F%E9%BE%99%E5%87%BB%E6%9D%80%E5%9C%B0%E5%BD%A2%E5%8F%98%E5%8C%96%E6%95%88%E6%9E%9C?sort_id=5287111)



人物残影 [wiki补充示例](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%BE%85%E5%81%9A%E6%95%88%E6%9E%9C/%E4%BA%BA%E7%89%A9%E6%AE%8B%E5%BD%B1?sort_id=5287113)

晶体(玻璃、钻石)效果  [wiki补充示例](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%BE%85%E5%81%9A%E6%95%88%E6%9E%9C/%E6%99%B6%E4%BD%93(%E7%8E%BB%E7%92%83%E3%80%81%E9%92%BB%E7%9F%B3)%E6%95%88%E6%9E%9C?sort_id=5287116)

屏幕故障 [wiki补充示例](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%BE%85%E5%81%9A%E6%95%88%E6%9E%9C/%E5%B1%8F%E5%B9%95%E6%95%85%E9%9A%9C?sort_id=5287119)


#### 三、特殊效果
溶解：已完成 待优化调整 [点击查看wiki](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E7%89%B9%E6%AE%8A%E6%95%88%E6%9E%9C/%E6%BA%B6%E8%A7%A3)

屏障: 已完成 待优化调整、补充细节 [点击查看wiki](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E7%89%B9%E6%AE%8A%E6%95%88%E6%9E%9C/%E5%B1%8F%E9%9A%9C)

冰冻：效果极差 shader代码已提交 [点击查看wiki](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E7%89%B9%E6%AE%8A%E6%95%88%E6%9E%9C/%E5%86%B0%E5%86%BB?sort_id=5229838)

待做 冰冻、玻璃、模糊、屏幕后处理、激光、画笔

#### 四、后处理相关
场景全局扫描效果 已完成部分 [点击查看wiki](https://gitee.com/ZhiJianShuShu/shader-demo-collection/wikis/%E5%90%8E%E5%A4%84%E7%90%86/%E5%9C%BA%E6%99%AF%E5%85%A8%E5%B1%80%E6%89%AB%E6%8F%8F%E6%95%88%E6%9E%9C)


#### 五、真实渲染（PBR)

待做 研究 NPR + PBR 的混合的渲染，真实渲染





待更新......
