﻿#if UNITY_EDITOR
using System.IO;
using UnityEditor;
using UnityEngine;

namespace ShaderTools
{
    public sealed class ShaderTemplateEditorWindow
    {
#region [MenuItem]
        [MenuItem("Assets/Create/Shader/Unlit URP Shader")]
        static void UnlitURPShader()
        {
            CreatShaderAsset("UnlitURPShader");
        }

        // [MenuItem("Assets/Create/Shader/Simple Lit URP Shader")]
        // static void SimpleLitURPShader()
        // {
        //     CreatShaderAsset("SimpleLitURPShader");
        // }
#endregion

#region [生成Shader文件]
        static void CreatShaderAsset(string shaderName)
        {
            string folderPath = string.Empty;
            string guid = Selection.assetGUIDs[0];
            folderPath = AssetDatabase.GUIDToAssetPath(guid);
            if (!AssetDatabase.IsValidFolder(folderPath))
                folderPath = Path.GetDirectoryName(folderPath);
            var path = AssetDatabase.GenerateUniqueAssetPath(string.Format("{0}/{1}.shader", folderPath, shaderName));
            AssetDatabase.CopyAsset(string.Format("Assets/Editor/URPShaderCreat/Template/{0}.shader", shaderName), path);
            // var contents = File.ReadAllText(AssetDatabase.GUIDToAssetPath("9d34865c54602e641a90c982de79b7a2"));
            // ProjectWindowUtil.CreateAssetWithContent(path, contents);
        }
#endregion
    }
}
#endif