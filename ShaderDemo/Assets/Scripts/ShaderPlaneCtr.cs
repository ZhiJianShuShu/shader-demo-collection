using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// shader面板控制器(最好做成合集或者通用的，不然就单个例子都写一个)
/// </summary>
public class ShaderPlaneCtr : MonoBehaviour
{

    public Transform mstTran;
    private Material DissolveMat;
    public float times = 1;
    private float waitTime = 0;

    private bool isStart = false;

    private Event doEvent;
    
    // Start is called before the first frame update
    void Start()
    {
        if (mstTran)
        {
            DissolveMat = mstTran.GetComponent<SkinnedMeshRenderer>().material;
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.StartDeath();
    }

    private void StartDeath(bool stop = false)
    {
        if (stop)
        {
            isStart = false;
            waitTime = 0;
            return;
        }
         // 有空抽封装下
        if (isStart)
        {
            waitTime += Time.deltaTime;
            DissolveMat.SetFloat("_Dissolve",waitTime/times);
            if (waitTime/times >= times)
            {
                isStart = false;
                waitTime = 0;
            }   
        }
    }


    /// <summary>
    /// 点击死亡
    /// </summary>
    public void OnClickDeath()
    {
        if (DissolveMat)
        {
            isStart = true;
        }
        
    }

    /// <summary>
    /// 重置
    /// </summary>
    public void OnClickReset()
    {
        if (DissolveMat)
        {
            this.StartDeath(true);
            DissolveMat.SetFloat("_Dissolve",0);
        }
    }
}