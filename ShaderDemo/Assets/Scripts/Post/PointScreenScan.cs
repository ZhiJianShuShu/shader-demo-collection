using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointScreenScan : PostEffectBase
{
    public float moveSpeed;
    public float timeScale;

    public float _Width;
    public Color _BgColor;
    public Color _MeshColor;
    public float _MeshLineWidth;
    public float _MeshWidth;
    public float _Smoothness;


    private float curTime;
    private Vector3 clickPoint = Vector3.zero;
    private Camera pointCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_Material)
        {
            this.SetBaseData();
            curTime += Time.deltaTime;
            this.GetPoint();
            
        }
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        if (_Material)
        {
            _Material.SetFloat("_Width", _Width);
            _Material.SetColor("_BgColor", _BgColor);
            _Material.SetColor("_MeshColor", _MeshColor);
            _Material.SetFloat("_MeshLineWidth", _MeshLineWidth);
            _Material.SetFloat("_MeshWidth", _MeshWidth);
            _Material.SetFloat("_Smoothness", _Smoothness);
            Graphics.Blit(src, dest, _Material);
        }
    }

    /// <summary>
    /// 设置默认的基础信息(摄像机参数不变的情况下)
    /// </summary>
    private void SetBaseData()
    {
        pointCamera = this.transform.GetComponent<Camera>();
        pointCamera.depthTextureMode = DepthTextureMode.Depth;
        pointCamera.depthTextureMode = DepthTextureMode.DepthNormals;

        // 摄像机的宽高比
        float aspect = pointCamera.aspect;
        // 远裁剪面
        float farClipPlaneDistance = pointCamera.farClipPlane;
        // 摄像机的广角
        float fov = pointCamera.fieldOfView;

        // 获取投影摄像机的四个边缘的向量 

        // Mathf.Deg2Rad 度转弧度
        Vector3 midUp = Mathf.Tan(fov / 2 * Mathf.Deg2Rad) * farClipPlaneDistance * pointCamera.transform.up;

        Vector3 midRight = Mathf.Tan(fov / 2 * Mathf.Deg2Rad) * farClipPlaneDistance * pointCamera.transform.right * aspect;

        // farPlaneMid 沿摄像机方向 从近裁剪面到远裁剪面的一条向量
        // 理解为镜头中间的向量
        Vector3 farPlaneMid = pointCamera.transform.forward * farClipPlaneDistance;

        // 理解为摄像机锥体的四条向量边
        Vector3 bottomLeft = farPlaneMid - midUp - midRight;
        Vector3 bottomRight = farPlaneMid - midUp + midRight;
        Vector3 upLeft = farPlaneMid + midUp - midRight;
        Vector3 upRight = farPlaneMid + midUp + midRight;

        Matrix4x4 frustumCorner = new Matrix4x4();
        frustumCorner.SetRow(0, bottomLeft);
        frustumCorner.SetRow(1, bottomRight);
        frustumCorner.SetRow(2, upRight);
        frustumCorner.SetRow(3, upLeft);

        // 除非镜头参数修改，这个矩阵值几乎是不变的
        _Material.SetMatrix("_FrustumCorner", frustumCorner);

        // 从摄像机空间到世界空间变换的矩阵
        _Material.SetMatrix("_CamToWorld", pointCamera.cameraToWorldMatrix);

        // 获取远裁剪面设置
        _Material.SetFloat("_CamFar", farClipPlaneDistance);
    }

    /// <summary>
    /// 获取射线点击
    /// </summary>
    private void GetPoint()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit))
        {
            curTime = 0;
            clickPoint = hit.point;
        }
        _Material.SetVector("_ClickPoint", clickPoint);
        _Material.SetFloat("_Range", curTime * moveSpeed);
    }
}
