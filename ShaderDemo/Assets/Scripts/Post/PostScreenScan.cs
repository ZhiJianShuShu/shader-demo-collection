using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostScreenScan : PostEffectBase
{
    public Color _Color;

    public float maxDistance = 0.5f;

    public float timeScale = 0.5f;
    public float width = 0.1f;
    private float distancce = 0;
    // Start is called before the first frame update
    void Start()
    {
        Camera.main.depthTextureMode = DepthTextureMode.Depth;
    }

    // Update is called once per frame
    void Update()
    {
        distancce += Time.deltaTime * timeScale;
        if (distancce >= maxDistance)
        {
            distancce = 0;
        }
        if (_Material)
        {
            _Material.SetFloat("_Distance",distancce);
        }
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest) {
        if (_Material)
        {
            _Material.SetFloat("_Width",width);
            _Material.SetColor("_Color",_Color);
            Graphics.Blit(src,dest,_Material);
        }
    }
}
