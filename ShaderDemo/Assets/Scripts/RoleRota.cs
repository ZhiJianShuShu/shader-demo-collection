using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoleRota : MonoBehaviour
{

    public List<Transform> roleList;
    public Transform tranCamera;

    public Transform tranLight;

    public float speed;
    public float lightSpeed;

    private int curIdx = 0;  //当前第几个
    private float curY = 0;  // 当前旋转的y轴
    private float lightY = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (curIdx >= roleList.Count)
        {
            return;
        }
        if (roleList.Count > 0 && curIdx <= roleList.Count -1)
        {
            Transform curTran = roleList[curIdx];
            if ( !curTran.gameObject.activeSelf)
            {
               curTran.gameObject.SetActive(true); 
            }
            if (curY <= 360)
            {
                curY += speed * Time.deltaTime;
                curTran.Rotate(Vector3.up * speed * Time.deltaTime);
            }
            else
            {
                if (lightY <= 360)
                {
                    lightY += lightSpeed * Time.deltaTime;
                    tranLight.Rotate(Vector3.up * lightSpeed * Time.deltaTime,Space.World);
                }
                else
                {
                    curY = 0;
                    lightY = 0;
                    curIdx++;
                    if (curIdx <= roleList.Count - 1)
                    {
                        curTran.gameObject.SetActive(false);
                    }
                    
                    
                }
            }
            
        }
        
    }


}
