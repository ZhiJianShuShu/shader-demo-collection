// 屏障
Shader "PJD/Effect/EnergyHood"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _MainColor("MainColor",Color) = (1,1,1,1)
        // 菲涅尔描边
        _RimColor("RimColor",Color) = (1,1,1,1)        
        _RimValue("RimValue",Vector) = (1,1,1,1)  
        // 交接处颜色
        _HightLightFade("HightLight Fade",float) = 0    
        _HightLightColor("HightLight Color",Color) = (1,1,1,1)
       
        _Distort("Distort",float) = 0.5   // 扭曲强度
        _Tiling("Tiling",float) = 5       // uv 层级次数
    }
    SubShader
    {
        Tags 
        {      
            "RenderType"="Opaque" 
            "Queue"="Transparent"   
        }

        // 截屏
        GrabPass{"_GrabTex"}

        Pass
        {
            // 混合 把rim 描边内的黑色部分混合成透明
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 viewPos : TEXCOORD1;  //屏幕空间坐标
                float3 worldNormal : TEXCOORD2;  
                float3 viewDirection : TEXCOORD3;   //世界空间下观察方向(摄像机坐标-世界坐标)
            };

            float4 _MainTex_ST;
            float4 _MainColor;
            float _HightLightFade;
            float4 _HightLightColor;
            // rim 描边
            float4 _RimColor;
            float4 _RimValue;
            
            float _Distort;
            float _Tiling;

            sampler2D _MainTex;
            sampler2D _CameraDepthTexture;
            sampler2D _GrabTex;

            v2f vert (appdata v)
            {
                v2f o;
                // 本地坐标转换到世界坐标
                float3 worldPos = mul(unity_ObjectToWorld,v.vertex);

                // 将顶点从世界空间转换到观察空间（顶点在观察空间下的坐标值）
                // 即从摄像机方向看到的坐标 摄像机是右手坐标系
                // viewPos 顶点在观察空间下的坐标值 距离越远，值越小 0 ~ -
                o.viewPos = UnityObjectToViewPos(v.vertex);

                // 将屏幕空间转换到齐次裁剪空间
                o.vertex = mul(UNITY_MATRIX_P,float4(o.viewPos.xyz,1));  //UnityObjectToClipPos(v.vertex);
                
                o.uv.xy = v.uv;
                o.uv.zw = TRANSFORM_TEX(v.uv,_MainTex);

                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.viewDirection = normalize(_WorldSpaceCameraPos - worldPos);

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col;
                //fixed4 worldPos = mul(unity_ObjectToWorld,fixed4(0,0,0,0));
                

                //================= 外发光
                fixed3 N = normalize(i.worldNormal);
                fixed3 V = i.viewDirection;
                fixed NdotV = 1 - saturate(dot(N,V));
                fixed4 rim = _RimValue.x * pow(NdotV,_RimValue.y);
                rim *= _RimColor;
                //return rim;

                //================= 交接部位发光

                // 获取深度图做 外发光交互部分
                // 当前坐标点在屏幕下的uv坐标
                float2 screenUV = i.vertex.xy / _ScreenParams.xy;

                // 取当前uv 下的深度图信息
                fixed4 depthMap = tex2D(_CameraDepthTexture,screenUV);

                // 把非线性的深度图信息转到 0~1 的线性空间下
                //depth 为摄像机从近裁剪面到 远裁剪的距离，距离越远 depth 越大 1 ~ 
                fixed depth = LinearEyeDepth(depthMap.r);

                // viewPos 顶点在观察空间下的坐标值 越靠近摄像机值越小 viewPos.z < 0
                // hightlight 不透明物体在内部  viewPos.z 离摄像机更近，值更小,则有 depth + i.viewPos.z < 0
    
                fixed4 hightlight = depth + i.viewPos.z;
                // _HightLightFade 调整距离
                hightlight *= _HightLightFade;

                // 1 - 求反，得到靠近的部分为 亮部
                hightlight = saturate(1 - hightlight);
                hightlight *= _HightLightColor;
                //return hightlight;

             
                //================= uv 流动
                fixed4 mainMap = tex2D(_MainTex,i.uv.zw + half2(0,_Time.y * _RimValue.z));
                mainMap.rgb = mainMap * _MainColor;
                mainMap.a *= _MainColor.a;
                //return mainMap;

                //================= 屏幕扭曲
                float2 distortUV = lerp(screenUV,mainMap.rr,_Distort);
                fixed4 opaqueTex = tex2D(_GrabTex,distortUV);
                fixed4 distort = half4(opaqueTex.rgb,1);

                // 拆分多层做流动效果 _Tiling 层级数量
                half flowMask = frac(i.uv.y * _Tiling + _Time.y * _RimValue.w);
                
                distort *= flowMask;

                col = (rim + mainMap )* distort;
                col += hightlight;

                return col;
            }
            ENDCG
        }
    }
}
