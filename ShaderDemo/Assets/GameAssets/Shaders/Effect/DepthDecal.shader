// 深度贴花(可做脚印，投射的花纹等)
Shader "PJD/Effect/DepthDecal"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color",Color) = (1,0,0,1)
    }

    //URP
    SubShader
    {
        Tags 
        { 
            "Queue"="Transparent"
            "RenderType" = "Opaque" 
            "IgnoreProjector" = "True" 
            "RenderPipeline" = "UniversalPipeline" 
        }
        LOD 100

        Pass
        {
            Blend one one
            Name "Unlit"
            HLSLPROGRAM
            // Required to compile gles 2.0 with standard srp library
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"


            struct Attributes
            {
                float4 positionOS       : POSITION;
            };

            struct Varyings
            {
                float4 positionCS       : SV_POSITION;
                float fogCoord          : TEXCOORD0;
                // 观察空间下坐标
                float3 viewPos          : TEXCOORD1;
            };

            CBUFFER_START(UnityPerMaterial)
            half4 _Color;
            float4 _MainTex_ST;
            CBUFFER_END
            TEXTURE2D (_MainTex);SAMPLER(sampler_MainTex);
            TEXTURE2D (_CameraDepthTexture);SAMPLER(sampler_CameraDepthTexture);
            // #define smp _linear_clampU_mirrorV
            // SAMPLER(smp);

            Varyings vert(Attributes v)
            {
                Varyings o = (Varyings)0;

                o.positionCS = TransformObjectToHClip(v.positionOS.xyz);

                o.fogCoord = ComputeFogFactor(o.positionCS.z);
                // 把本地坐标转到 屏幕空间下
                o.viewPos = TransformWorldToView(TransformObjectToWorld(v.positionOS.xyz).xyz);

                return o;
            }

            half4 frag(Varyings i) : SV_Target
            {

                // 获取当前坐标点在屏幕上的像素位置
                float2 screenUV = i.positionCS.xy / _ScreenParams.xy;

                half4 depthTex = SAMPLE_TEXTURE2D(_CameraDepthTexture,sampler_CameraDepthTexture,screenUV);
                // 获取当前像素 所在的深度位置
                half depthZ = LinearEyeDepth(depthTex.x,_ZBufferParams);


                // 构建深度图上的像素在 观察空间下的坐标
                // 将像素位置 转换成 坐标位置
                float4 depthPos = 1;
                depthPos.xy = i.viewPos.xy * depthZ / -i.viewPos.z;
                depthPos.z = depthZ;

                //将  观察空间 -> 世界空间 -> 本地空间
                float4 depthWorldPos = mul(unity_CameraToWorld,depthPos);
                float4 depthLocalPos = mul(unity_WorldToObject,float4(depthWorldPos.xyz,1));

                // cube 默认本地空间坐标 在 -0.5~0.5 之间 转到 0~1 
                float2 uv = depthLocalPos.xz + 0.5;  // 调整uv位置

                half4 col = SAMPLE_TEXTURE2D(_MainTex,sampler_MainTex,uv);
                col *= _Color;

                col.rgb = MixFog(col.rgb, i.fogCoord);
                return col;
            }
            ENDHLSL
        }
    }

    SubShader
    {
        Tags 
        {  
            "RenderType"="Opaque" 
            "Queue"="Transparent"  
        }
        LOD 100

        Pass
        {
            Blend one one
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                // 观察空间下坐标
                float3 viewPos : TEXCOORD0;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _CameraDepthTexture;
            float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                
                // 把本地坐标转到 屏幕空间下
                o.viewPos = UnityObjectToViewPos(v.vertex);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

                // 获取当前坐标点在屏幕上的像素位置
                float2 screenUV = i.vertex.xy / _ScreenParams.xy;

                fixed4 depthTex = tex2D(_CameraDepthTexture,screenUV);
                // 获取当前像素 所在的深度位置
                fixed depthZ = LinearEyeDepth(depthTex.x);

                // 构建深度图上的像素在 观察空间下的坐标
                // 将像素位置 转换成 坐标位置
                float4 depthPos = 1;
                depthPos.xy = i.viewPos.xy * depthZ / -i.viewPos.z;
                depthPos.z = depthZ;

                //将  观察空间 -> 世界空间 -> 本地空间
                float4 depthWorldPos = mul(unity_CameraToWorld,depthPos);
                float4 depthLocalPos = mul(unity_WorldToObject,float4(depthWorldPos.xyz,1));

                // cube 默认本地空间坐标 在 -0.5~0.5 之间 转到 0~1 
                float2 uv = depthLocalPos.xz + 0.5;  // 调整uv位置

                // sample the texture
                fixed4 col = tex2D(_MainTex,uv);
                col *= _Color;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
