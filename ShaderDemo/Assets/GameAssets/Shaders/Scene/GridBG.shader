Shader "PJD/Scerne/GridBG"
{
    Properties
    {
        _Repeat("Repeat",float) = 5
        _Color("Color",Color) = (0.5,0.5,0.5,1)
        [Enum(UnityEngine.Rendering.CullMode)]_Cull("Cull",int) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        Cull [_Cull]

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 localPos : TEXCOORD1;
            };

            float _Repeat;
            float4 _Color;


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.localPos = v.vertex;
                o.uv = v.uv * _Repeat;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col;
                float2 uv = floor(i.uv*2)*0.5;
                float checker = frac(uv.x + uv.y) * 2;

                fixed mask = i.localPos.y + 0.6;
                col = checker * mask;
                col *= _Color;
                return col;
            }
            ENDCG
        }
    }
}
