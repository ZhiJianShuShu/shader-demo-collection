//水
Shader "PJD/Scerne/Water"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DepthValue("DepthValue",float) = 1
        _WaterColor01("WaterColor01",Color) = (1,1,1,1) //浅水区
        _WaterColor02("WaterColor02",Color) = (1,1,1,1) // 深水区
    }
    SubShader
    {
        Tags 
        {   "Queue"="Transparent"
            "RenderType"="Transparent" 
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float4 _WaterColor01;
            float4 _WaterColor02;
            sampler2D _CameraDepthTexture;
            float _DepthValue;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
               
                //========== 水深

                // 像素在屏幕空间下uv坐标
                fixed2 screenUV = i.vertex.xy / _ScreenParams.xy;

                // 深度图获取深度信息
                fixed4 depthTex = tex2D(_CameraDepthTexture,screenUV);
                // 转到0~1 的线性空间下 离镜头越近，值越小
                fixed depthScene = LinearEyeDepth(depthTex.r);
                depthScene *= _DepthValue;
                // return depthScene;
                // fixed depth = depthScene + i.vertex.z;
                // depth *= _DepthValue;
                fixed4 waterColor = lerp(_WaterColor01,_WaterColor02,depthScene);
                return waterColor;

                // 水面颜色

                // 水面泡沫

                // 水面反射

                // 水面高光

                // 水底焦散

                // 水下扭曲 

                return 1;
            }
            ENDCG
        }
    }
}
