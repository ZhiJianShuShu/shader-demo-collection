Shader "PJD/Role/Cartoon/Seele/Seele_C1_QT/SeeleEye"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _OffsetX("OffsetX",Range(-1,1)) = 0
        _OffsetY("OffsetY",Range(-1,1)) = 0
        _Hor("Hor",int) = 1
        _Ver("Ver",int) = 1
        _Index("Index",int) = 1
        // [Enum(UnityEngine.Rendering.BlendMode)]_SrcBlend("SrcBlend ",int) = 0
        // [Enum(UnityEngine.Rendering.BlendMode)]_DstBlend("DstBlend ",int) = 0
    }
    SubShader
    {
        Tags 
        {   
            "RenderType"="Transparent" 
            "Queue"="Transparent" 
        }

        Pass
        {
            //Blend [_SrcBlend] [_DstBlend]
            Blend SrcAlpha OneMinusSrcAlpha
            Offset [_OffsetX],[_OffsetY]
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            half _Hor;
            half _Ver;
            half _Index;


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                half oneH = 1/_Ver; // 单个水平位移(缩放)
                half oneV = 1/_Hor; // 单个竖直位置
                half x = _Index%_Ver * oneH;  
                half y = (_Hor - ceil((_Index+1)/_Hor)) * oneV;
                o.uv = v.uv * half2(oneH,oneV) + half2(x,y); //TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
