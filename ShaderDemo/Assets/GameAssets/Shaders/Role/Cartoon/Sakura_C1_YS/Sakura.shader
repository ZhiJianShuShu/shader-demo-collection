Shader "PJD/Role/Cartoon/Sakura/Sakura_C1_YS/Sakura"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _LightTex("LightTex",2D) = "white" {}

        // 描边
        [Header(Outline)]
        _OutlineColor("OutlinneColor",Color) = (0,0,0,1)
        _OutlineWidth("OutlineWidth",Range(0,1)) = 0.1
        _UniformWidth("UniformWidth",Range(0,1)) = 0.1 //

        // [Header(Color)]
        [Header(Diffuse)]
        _HColor("Highlight Color",Color) = (1,1,1,1)   //亮部颜色
        _SColor("shadow Color",Color) = (0.5,0.5,0.5,1)  //阴影部分颜色
        _RampThreshold("RampThreshold",Range(0,1)) = 0.8		//明暗边界阀值
        _RampSmooth("RampSmooth",Range(0,1)) = 0.1				// 明暗边界平滑过渡
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        // 描边
        Pass
        {
            Cull Front
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 color  : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float _OutlineWidth;
            float4 _OutlineColor;
            float _UniformWidth;

            v2f vert (appdata v)
            {
                v2f o;
               // 描边本身的宽度是不会变化的，但是随着时距会有近大远小的视觉效果
                // 如果需要保持不变，则需要根据时距的变化加上对应的值，则原来宽度+视距的长度，可表示效果
                // 求出相机与顶点之间的距离
                float3 worldPos = mul(unity_ObjectToWorld,v.vertex.xyz);
                float3 distance = length(_WorldSpaceCameraPos - worldPos);

                //distance = 1 时，保持近大远小
                //distance = distance 时，保持粗细不变
                // _UniformWidth = 0时，使用默认近大远小 ，否则保持粗细不变
                distance = lerp(1,distance,_UniformWidth);
                float3 pos = v.vertex.xyz;
                float3 width = normalize(v.normal) * _OutlineWidth *0.01;
                width *= distance;
                width *= v.color.a;  //模型的顶点色 a 控制模型描边的粗细
                v.vertex.xyz += width;
                
                o.vertex = UnityObjectToClipPos(v.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _OutlineColor;
            }
            ENDCG
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
