Shader "PJD/Paint/RolePaint"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_PaintTex("PaintTex",2D) = "white" {}
		_PaintTex2("PaintTex2",2D) = "white" {}
		_PaintTex3("PaintTex3",2D) = "white" {}
		_PaintTex4("PaintTex4",2D) = "white" {}
		_PaintLimit("PaintLimit",Vector) = (0.8,0.5,0.3,0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : Normal;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 worldNormal : TEXCOORD1;
				float3 viewPos : TEXCOORD2;
				//float3 worldPos : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			sampler2D _PaintTex;
			sampler2D _PaintTex2;
			sampler2D _PaintTex3;
			sampler2D _PaintTex4;
			float4 _PaintLimit;

            v2f vert (appdata v)
            {
                v2f o;
				o.viewPos = mul(UNITY_MATRIX_P,float4(v.vertex.xyz,1));
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				//fixed2 uv = i.viewPos.y / _ScreenParams.xy;
				fixed2 uv = i.uv * _MainTex_ST.xy + _MainTex_ST.zw;
                fixed4 col = tex2D(_MainTex, uv);
				fixed4 lineTex = tex2D(_PaintTex, uv);
				fixed4 lineTex2 = tex2D(_PaintTex2, uv);
				fixed4 lineTex3 = tex2D(_PaintTex3, uv);
				fixed4 lineTex4 = tex2D(_PaintTex4, uv);

				// 仅用lamber

				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float3 normalDir = normalize(i.worldNormal);
				fixed NdotL = dot(normalDir,lightDir) * 0.5 + 0.5;

				if(NdotL > _PaintLimit.x)
				{
					return lineTex.r * NdotL;
				}
				if(NdotL > _PaintLimit.y)
				{
					return lineTex2.r * NdotL;
				}
				
				if(NdotL > _PaintLimit.z)
				{
					return lineTex3.r * NdotL;
				}
				if(NdotL >= _PaintLimit.w)
				{
					return lineTex4.r * NdotL;
				}

                return col;
            }
            ENDCG
        }
    }
}
