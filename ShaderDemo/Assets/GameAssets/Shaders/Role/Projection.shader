// 全息投影 (实际就是半透明舞台加扰动)
Shader "PJD/Role/Projection"
{
    Properties
    {
        _MainTex("Texture",2D) = "white" {}  // 波纹 做上下移动变化线条
        _SpeedY("SpeedY",Range(0,3)) = 1    // 波动移动速度
        _RimColor("RimColor",Color) = (1,1,1,1)   // 外描边颜色
        _Color("Color",Color) = (1,1,1,1) 
        _RimValue("RimValue",Vector) = (1,1,1,1) // rim 系数 
        _Dissolve("Dissolve",Range(0,1)) = 0.1
    }
    SubShader
    {
        Tags 
        {  
            "Queue"="Transparent"
            "RenderType"="Transparent"  
        }
        //Cull off
        //ZWrite off
        Blend one one // 混合做透明效果
        //Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                 float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 worldNormal : TEXCOORD1;
                float3 worldPos : TEXCOORD2;
                float3 localPos : TEXCOORD3;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _SpeedY;
            float4 _RimColor;
            float4 _RimValue;
            float _Dissolve;
            float4 _Color;

            v2f vert (appdata v)
            {
                v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;// TRANSFORM_TEX(v.uv, _MainTex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld,v.vertex);
                o.localPos = o.vertex;
				return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //return ;
                //边缘外发光 + 上下移动线条扰动 + 部分边缘擦除
                 // rim 发光描边
                fixed4 rimColor = 0;
                fixed3 N = normalize(i.worldNormal);
                fixed3 V = normalize(_WorldSpaceCameraPos.xyz - i.worldPos);
                fixed NdotV = 1 - saturate(dot(N,V));

                rimColor = _RimValue.x * pow(NdotV,_RimValue.y);
                rimColor *= _RimColor;
                rimColor *= _RimValue.z;

                // 竖直方向移动条纹
                // fixed2 uv = fixed2(0,_Time.y * _SpeedY * i.uv.y);
                // fixed4 mainTex = tex2D(_MainTex,uv);

                //考虑做一定时间 左右偏移的 信号偏差效果 + 或偏移至消失
                fixed2 uv = fixed2(0,frac(i.worldPos.y + _Time.y * _RimValue.z));
                fixed4 main = tex2D(_MainTex,uv);
                fixed4 col = main * _Color;
                //return col;
                // col *= _RimColor;
                // clip(col.a -_Dissolve);
                col += rimColor;
                return col;
            }
            ENDCG
        }
    }
}
