﻿// 效果性 溶解(渐显) 
Shader "PJD/Role/EffectDissolve"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        
        _DissolveTex("DissolveTex",2D) = "White" {}
        _Dissolve("Dissolve",Range(0,1)) = 0
        _RampTex("RampTex",2D) = "white" {}
        _NormalTex("NormalTex",2D) = "white" {}
        _IceValue("IceValue",Vector) = (0.5,0.5,0.5,0.5)
    }
    SubShader
    {
        Tags 
        {   
            "Queue"="Transparent"
            "RenderType"="Transparent" 
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;

            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 lightDir : TEXCOORD1;
                float3 viewDir : TEXCOORD2;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _NormalTex;
            sampler2D _DissolveTex;
            float _Dissolve;
            sampler2D _RampTex;
            float4 _IceValue;


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

                // 获取法线 切线 副切线(为了转换法线纹理)
                fixed3 worldNormal = UnityObjectToWorldNormal(v.normal);
                float3 worldTangent = UnityObjectToWorldDir(v.tangent.xyz);
                float3 worldBitangent = cross(worldNormal,worldTangent) * v.tangent.w;

                // 世界空间转切线空间
                float3x3 worldToTangent = float3x3(worldTangent,worldBitangent,worldNormal);

                // _WorldSpaceCameraPos.xyz - mul(unity_ObjectToWorld,v.vertex)
                o.lightDir = mul(worldToTangent,WorldSpaceLightDir(v.vertex));
                o.viewDir = mul(worldToTangent,WorldSpaceViewDir(v.vertex));
    

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);

                fixed4 dissolveTex = tex2D(_DissolveTex,i.uv);

                fixed temp = step(dissolveTex.r,_Dissolve);
                fixed4 rampTex = tex2D(_RampTex,i.uv);
                fixed4 finai = 0;
                if(temp <= 0)
                {
                    finai = col;
                }
                else
                {
                    fixed3 tangentLightDir = normalize(i.lightDir);
                    fixed3 tangentViewDir = normalize(i.viewDir);

                    // 获取法线纹理
                    // 这里在切线空间下计算
                    // 切线空间下法线
                    float3 tangentNormal = UnpackNormal(tex2D(_NormalTex,i.uv));
                    tangentNormal.xy *= _IceValue.b;
                    tangentNormal.z = sqrt(1.0 - saturate(dot(tangentNormal.xy,tangentNormal.xy)));

                    fixed3 diffuse = _LightColor0.rgb * max(0,dot(tangentNormal,tangentLightDir)) * rampTex * _IceValue.g;

                    fixed halfDir = normalize(tangentViewDir + tangentLightDir);
                    fixed3 specular = _LightColor0.rgb * pow(max(0,dot(tangentNormal,halfDir)),_IceValue.a); 

                    //finai.rgb = col * temp * _IceValue.r + (diffuse + specular) * temp;

                    finai.rgb = col * temp * _IceValue.r + rampTex * temp * _IceValue.g;
                }


                return finai;
            }
            ENDCG
        }
    }
}
