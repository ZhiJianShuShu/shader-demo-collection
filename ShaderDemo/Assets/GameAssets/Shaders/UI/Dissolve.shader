﻿// 溶解效果
Shader "PJD/UI/UIDissolve"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _DissolveTex("DissolveTex",2D) = "White" {}
        _Dissolve("Dissolve",Range(0,1.1)) = 0
        _RampTex("RampTex",2D) = "white" {}
        _Width("Width",Range(0.01,0.1)) = 0.01

        [Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
    }
    SubShader
    {
        Tags 
        {   
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True" 
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _DissolveTex;
            float _Dissolve;
            sampler2D _RampTex;
            float _Width;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);

                fixed4 dissolveTex = tex2D(_DissolveTex,i.uv);

                clip(dissolveTex.r - _Dissolve);

                fixed4 rampTex = tex2D(_RampTex,smoothstep(_Dissolve,_Dissolve + _Width,dissolveTex.r));

                col *= rampTex;

                #ifdef UNITY_UI_ALPHACLIP
                    clip (color.a - 0.001);
                #endif

                return col;
            }
            ENDCG
        }
    }
}
