Shader "PJD/UI/ScaleUI"
{
    Properties
    {
		_MainTex("MainTex",2D) = "white" {}
        _Scale("Scale",Range(0,3)) = 0.1

	 	 [Header(compVal)]      // 对比方式
        _StencilComp ("StencilComparison", Float) = 8  
        
        [Header(curRef)] // 当前的模板值
        _Stencil ("StencilID", Float) = 0
        
        [Header(passType)]  // 通过后的操作
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255

        _ColorMask ("Color Mask", Float) = 15

        _SecondRang("SecondRang",Range(0,1)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        Cull off
		Blend SrcAlpha OneMinusSrcAlpha  // 混合 令其带透明通道
		 //屏幕抓取
        GrabPass
        {
            "_GrabTex"  // 对当前抓取的屏幕缓存进行命名（可以理解成缓存的帧画面吧？） 则当有其他使用的时候会直接使用已经抓取过的 _GrabTex ，所以就不会多次抓取
        }

		//   // 模板测试相关
        // Stencil
        // { 
        //     Ref [_Stencil]              // 关键字 当前写入到模板缓存中的值
        //     Comp [_StencilComp]         // 对比
        //     Pass [_StencilOp]           // 对比通过之后的处理方式  keep 保持不变  因为做的是遮罩效果，则当有满足缓存区间内的内容保持渲染
        //     ReadMask [_StencilReadMask]
        //     WriteMask [_StencilWriteMask]
        // }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

			 sampler2D _GrabTex;
			 float4 _GrabTex_ST;
			 fixed _Scale;


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
               
				fixed2 screenUV = i.vertex.xy / _ScreenParams.xy;

				float2 whScale = float2(_ScreenParams.x/_ScreenParams.y,1);
				float2 center = fixed2(0.5,0.5);

				float2 dir = center - screenUV;
				float distance = length(center - i.uv.xy);
				distance = 0.5 - distance;
				fixed4 grab = tex2D(_GrabTex,screenUV + dir * _Scale * distance); 
                return grab;
            }
            ENDCG
        }
    }
}
