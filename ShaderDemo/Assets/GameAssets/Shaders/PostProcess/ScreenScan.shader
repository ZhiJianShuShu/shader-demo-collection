Shader "PJD/PostProcess/ScreenScan"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color",Color) = (0,1,0,1)
        _Width("Width",Range(0,1)) = 0.1
        _Distance("Distance",float) = 0
        _CamFar("CamFar",float) = 500
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _Color;
            float _Width;
            float _Distance;
            float _CamFar;
            sampler2D _CameraDepthTexture;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // 取深度图 转到 0~1 区间
                float depthTex = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture,i.uv);
                // 需要距离足够远看起来从 0~1 才会有明显效果
                float depth = Linear01Depth(depthTex);
                //return depth;
                fixed4 col = tex2D(_MainTex, i.uv);
 
                if(depth < _Distance && depth > _Distance - _Width/_CamFar && depth < 1)
                {
                    // 颜色做插值
                    // val随时间变化从
                    fixed val = 1 - (_Distance -depth )/(_Width/_CamFar);
                    return lerp(col,_Color,val);
                }

                return col;
            }
            ENDCG
        }
    }
}
