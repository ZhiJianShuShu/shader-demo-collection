Shader "PJD/PostProcess/PointScreenScan"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Range("Range",float) = 0
        _Width("Width",float) = 1
        _BgColor("BgColor",Color) = (1,1,1,1)
        _MeshColor("MeshColor",Color) = (1,1,1,1)
        _MeshLineWidth("MeshLineWidth",float) = 0.3
        _MeshWidth("MeshWidth",float) = 1
        _Smoothness("SeamBlending",Range(0,0.5))=0.25
    }
    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 interpolatedRay : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Range;
            float _Width;
            float4 _BgColor;
            float4 _MeshColor;
            float _MeshLineWidth;
            float _MeshWidth;
            float _Smoothness;
            float4x4 _CamToWorld;
            float3 _ClickPoint;
            float4x4 _FrustumCorner;

            sampler2D_float _CameraDepthTexture;
            sampler2D _CameraDepthNormalsTexture;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;

                // 根据屏幕uv 取到对应的向量
                int rayIndex;
                if(v.uv.x<0.5&&v.uv.y<0.5){
                    rayIndex = 0;
                    }else if(v.uv.x>0.5&&v.uv.y<0.5){
                    rayIndex = 1;
                    }else if(v.uv.x>0.5&&v.uv.y>0.5){
                    rayIndex = 2;
                    }else{
                    rayIndex = 3;
                }
                o.interpolatedRay = _FrustumCorner[rayIndex];


                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

                // 取到深度图和 深度图的法线信息
                float tempDepth;
                float3 normal;
                DecodeDepthNormal(tex2D(_CameraDepthNormalsTexture, i.uv), tempDepth, normal);
                // 转到世界空间下
                normal = mul( (float3x3)_CamToWorld, normal);
                normal = normalize(max(0, (abs(normal) - _Smoothness)));

                fixed4 col = tex2D(_MainTex, i.uv);

                float depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
                float linearDepth = Linear01Depth(depth);

                // 深度图中的像素点在世界坐标下的位置
                float3 pixelWorldPos = _WorldSpaceCameraPos + linearDepth * i.interpolatedRay;

                // 深度图中像素的世界坐标到鼠标点击的距离
                float pixelDistance = distance(pixelWorldPos , _ClickPoint);

                // 深度图中像素的世界坐标到鼠标点击 的向量 （得到了圆形）
                float3 pixelDir = pixelWorldPos - _ClickPoint;

                // 待补充注释
                float3 modulo = pixelWorldPos - _MeshWidth * floor(pixelWorldPos/_MeshWidth);
                modulo = modulo/_MeshWidth;

                float3 meshCol = smoothstep(_MeshLineWidth,0,modulo) + smoothstep(1-_MeshLineWidth,1,modulo);
                fixed4 _meshCol = lerp(_BgColor,_MeshColor,saturate(dot(meshCol,1-normal)));

                if(_Range - pixelDistance > 0 && _Range - pixelDistance <_Width && linearDepth < 1){
                    fixed scanPercent = 1 - (_Range - pixelDistance)/_Width;
                    col = lerp(col,_meshCol,scanPercent);
                }

                return col;
            }
            ENDCG
        }
    }
}
